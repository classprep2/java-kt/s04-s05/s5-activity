package com.zuitt.example;


public class Main {

    // I searched for function in Java, it always says method, I guess in Java methods are used for functions
    public static void displayContacts(Contact contact){
        System.out.println(contact.getName());
        System.out.println("---------");
        System.out.println(contact.getName() + " has the following registered number:");
        System.out.print(contact.getContactNumber());
        System.out.println(contact.getName() + " has the following registered address:");
        System.out.println("my home in " + contact.getAddress());
    }

    public static void main(String[] args) {


        // Instantiate a new object named phonebook class
        Phonebook phonebook = new Phonebook();

        // Try isEmpty() method om java
//        if(phonebook.getContacts().isEmpty()){
//            System.out.print("Phonebook is Empty");
//        }

        // Contact contact = new Contact();
        // Instantiate two contacts from contact class - John Doe and Jane Doe
        Contact contact1 = new Contact("Jane Doe","+639162148573","Caloocan City");
        Contact contact2 = new Contact("John Doe","+639152468596","Quezon City");

        // Add both contacts to the phonebook
        phonebook.setContacts(contact1);
        phonebook.setContacts(contact2);

        if(phonebook.getContacts().isEmpty()){
            System.out.print("Phonebook is Empty");
        }
        else{
            phonebook.getContacts().forEach((contact) -> displayContacts(contact));
        }

    }


}
