package com.zuitt.example;

import java.util.ArrayList;

public class Phonebook {

    // instantiate a variable
    // an empty arraylist of contact objects
    private ArrayList<Contact> contacts = new ArrayList<Contact>();

    // default constructor
    public Phonebook() {};

    // parameterized constructor
    public Phonebook(Contact contact) {
        // add() method adds a specific element
        this.contacts.add(contact);
    }

    // getter
    public ArrayList<Contact> getContacts() {
        return contacts;
    }

    // setter
    public void setContacts(Contact contact) {
        this.contacts.add(contact);
    }

}
