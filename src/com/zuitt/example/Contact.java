package com.zuitt.example;

public class Contact {

    String name;
    String contactNumber;
    String address;

    // Default constructor
    public Contact(){}

    // Parameterized constructor
    public Contact(String name, String contactNumber, String address) {
        this.name = name;
        this.contactNumber = contactNumber;
        this.address = address;
    }

    // getters
    public String getName() {
        return name;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public String getAddress() {
        return address;
    }

    // setters
    public void setName(String name) {
        this.name = name;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;


    }

    public void setAddress(String address) {
        this.address = address;
    }


}
